package mp

import (
	"code.google.com/p/gogoprotobuf/proto"
	"errors"
	"fmt"
	"math"
	"net"
	"os"
	"runtime/debug"
	"sync"
	"time"
)

const (
	DiscoveryPort = 64502
	UpdatePort    = 64503
)

type Client struct {
	Name          string
	Level         string
	LastDiscovery int64
	LastUpdate    int64
	DiscoveryAddr *net.UDPAddr
	UpdateAddr    *net.UDPAddr
}

type Server struct {
	mutex         sync.Mutex
	running       bool
	clientId      int32
	externalIp    string
	clients       map[int32]Client
	discoveryConn *net.UDPConn
	updateConn    *net.UDPConn
	discoveryChan chan<- DiscoveryMessage
	updateChan    chan<- UpdateMessage
}

func NewServer(id int32, discoveryChan chan<- DiscoveryMessage, updateChan chan<- UpdateMessage) *Server {
	return &Server{
		clientId:      id,
		clients:       make(map[int32]Client, 20),
		discoveryChan: discoveryChan,
		updateChan:    updateChan,
	}
}

func (v1 *Vec3) DistVec(v2 *Vec3) float32 {
	return v1.Dist(v2.X, v2.Y, v2.Z)
}

func (v *Vec3) Dist(x, y, z float32) float32 {
	dx, dy, dz := v.X-x, v.Y-y, v.Z-z
	return float32(math.Sqrt(float64(dx*dx) + float64(dy*dy) + float64(dz*dz)))
}

func (s *Server) StartListening(externalIp string, discoveryPort, updatePort int) (err error) {
	s.externalIp = externalIp

	s.discoveryConn, err = makeConn(discoveryPort)
	if err != nil {
		return
	}
	s.updateConn, err = makeConn(updatePort)
	if err != nil {
		s.discoveryConn.Close()
		return
	}

	go s.listenDiscovery()
	go s.listenUpdate()
	go func() {
		c := time.Tick(5 * time.Second)
		for _ = range c {
			s.RemoveOldClients()
		}
	}()

	return
}

func (s *Server) Clients() (m map[int32]Client) {
	s.mutex.Lock()
	defer s.mutex.Unlock()

	m = make(map[int32]Client, len(s.clients))
	for id, client := range s.clients {
		m[id] = client
	}
	return
}

func (s *Server) RemoveOldClients() {
	s.mutex.Lock()
	defer s.mutex.Unlock()

	now := time.Now().Unix()
	for cid, client := range s.clients {
		if client.LastDiscovery < now-10 {
			delete(s.clients, cid)
		}
	}
}

func (s *Server) BroadcastUpdateFromClient(msg *UpdateMessage, fromId int32) {
	clients := s.Clients()

	var from Client
	var ok bool
	if from, ok = clients[fromId]; !ok {
		return
	}

	for id, client := range clients {
		if client.UpdateAddr == nil {
			continue
		}

		if client.Level != from.Level {
			continue
		}

		if id == fromId {
			continue
		}

		s.SendUpdate(msg, id)
	}
}

func (s *Server) BroadcastUpdate(msg *UpdateMessage, level string) {
	clients := s.Clients()

	for id, client := range clients {
		if client.UpdateAddr == nil {
			continue
		}

		if client.Level != level {
			continue
		}

		s.SendUpdate(msg, id)
	}
}

func (s *Server) SendUpdate(msg *UpdateMessage, clientId int32) (err error) {
	s.mutex.Lock()
	defer s.mutex.Unlock()

	if client, ok := s.clients[clientId]; ok {
		if client.UpdateAddr == nil {
			return errors.New(fmt.Sprintf("Client with id %d has no UpdateAddr", clientId))
		}
		err = writeMessage(s.updateConn, msg, client.UpdateAddr)
		return
	}

	return
}

func (s *Server) listenDiscovery() {
	buf := make([]byte, 256)
	for {
		msg := &DiscoveryMessage{}
		addr, err := readMessage(s.discoveryConn, buf, msg)
		if err != nil {
			stackTrace(err)
			continue
		}

		s.addClient(msg.GetClientId(), msg.GetDiscoveryData().GetLevel(), addr)

		select {
		case s.discoveryChan <- *msg:
		default:
		}

		msg.GetDiscoveryData().Ip = &s.externalIp
		msg.ClientId = &s.clientId

		writeMessage(s.discoveryConn, msg, addr)
	}
}

func (s *Server) listenUpdate() {
	buf := make([]byte, 4096)
	for {
		msg := &UpdateMessage{}
		addr, err := readMessage(s.updateConn, buf, msg)
		if err != nil {
			stackTrace(err)
			continue
		}

		var name string
		cid := msg.GetClientId()

		for _, obj := range msg.GetObjectData() {
			if obj.PlayerInitials != nil {
				name = *obj.PlayerInitials
			}
		}

		s.updateClient(cid, name, addr)

		select {
		case s.updateChan <- *msg:
		default:
		}
	}
}

func (s *Server) addClient(id int32, level string, discoveryAddr *net.UDPAddr) {
	s.mutex.Lock()
	defer s.mutex.Unlock()

	client := s.clients[id]
	client.Level = level
	client.LastDiscovery = time.Now().Unix()
	client.DiscoveryAddr = discoveryAddr

	s.clients[id] = client
}

func (s *Server) updateClient(id int32, name string, updateAddr *net.UDPAddr) {
	s.mutex.Lock()
	defer s.mutex.Unlock()

	if client, ok := s.clients[id]; ok {
		if name != "" {
			client.Name = name
		}
		client.LastUpdate = time.Now().Unix()
		client.UpdateAddr = updateAddr
		s.clients[id] = client
	}
}

func makeConn(port int) (conn *net.UDPConn, err error) {
	laddr := &net.UDPAddr{
		Port: port,
		IP:   net.ParseIP("0.0.0.0"),
	}
	conn, err = net.ListenUDP("udp4", laddr)
	return
}

func readMessage(conn *net.UDPConn, buf []byte, msg proto.Message) (addr *net.UDPAddr, err error) {
	n, addr, err := conn.ReadFromUDP(buf)
	if err != nil {
		return
	}

	err = proto.Unmarshal(buf[:n], msg)
	return
}

func writeMessage(conn *net.UDPConn, msg proto.Message, addr *net.UDPAddr) (err error) {
	data, err := proto.Marshal(msg)
	if err != nil {
		return
	}

	_, err = conn.WriteToUDP(data, addr)
	return
}

func stackTrace(err error) {
	fmt.Fprintln(os.Stderr, err)
	debug.PrintStack()
}
