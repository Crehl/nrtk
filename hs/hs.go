package hs

import (
	"bitbucket.org/Crehl/nrtk/level"
	"bitbucket.org/Crehl/nrtk/nrxml"
	"encoding/xml"
	"fmt"
	"io"
	"net/http"
)

type ScoreSet struct {
	Version  int
	Style    level.ScoreStyle
	LevelId  string
	Category int
	Scores   []Score
}

type Score struct {
	Initials     string
	Time         float32
	Score        int
	BiggestTrick int
	Date         string
	ScoreId      int
}

type xmlScoreSet struct {
	nrxml.Node
	Version int        `xml:"version,attr"`
	Style   int        `xml:"style,attr"`
	Scores  []xmlScore `xml:"Score"`
}

type xmlScore struct {
	nrxml.Node
}

func DownloadScores(server, version, level, group string, isTimeBased bool) (ss ScoreSet, err error) {
	itb := "1"
	if !isTimeBased {
		itb = "0"
	}

	url := fmt.Sprintf("%s/game_conn/score_get.php?v=%s&lsi=%s&fgsi=%s&itb=%s",
		server,
		version,
		level,
		group,
		itb)

	resp, err := http.Get(url)
	if err != nil {
		return
	}
	defer resp.Body.Close()

	return ParseScoreSet(resp.Body)
}

func ParseScoreSet(r io.Reader) (ss ScoreSet, err error) {
	xmlSS := xmlScoreSet{}

	decoder := xml.NewDecoder(r)
	err = decoder.Decode(&xmlSS)
	if err != nil {
		return
	}

	ss.Version = xmlSS.Version
	ss.Style = level.ScoreStyle(xmlSS.Style)

	nrxml.SetString(xmlSS.Strings, "level_id", &ss.LevelId)
	nrxml.SetInteger(xmlSS.Integers, "category", &ss.Category)

	ss.Scores = make([]Score, len(xmlSS.Scores))
	for i, xmlS := range xmlSS.Scores {
		score := Score{}
		nrxml.SetString(xmlS.Strings, "initials", &score.Initials)
		nrxml.SetFloat(xmlS.Floats, "time", &score.Time)
		nrxml.SetInteger(xmlS.Integers, "score", &score.Score)
		nrxml.SetInteger(xmlS.Integers, "biggest_trick", &score.BiggestTrick)
		nrxml.SetString(xmlS.Strings, "date", &score.Date)
		nrxml.SetInteger(xmlS.Integers, "score_id", &score.ScoreId)
		ss.Scores[i] = score
	}

	return
}
