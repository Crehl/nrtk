package ghost

type Entity struct {
	Id        int16
	Blueprint string
}

type EntityTick struct {
	Action Action
	Data   EntityData
}

type Tick struct {
	Entities map[*Entity]EntityTick
}

func (g *Ghost) TickList() []Tick {
	entities = make([]Entity, len(g.EntityTypes))
	for eid, bpidx := range g.EntityTypes {
		entities[eid] = Entity{
			Id:        eid,
			Blueprint: g.EntityNames[bpidx],
		}
	}

	ticks = make([]Tick, len(g.TimedTicks)+1)

	di := 0 // data index
	t := 0

	for ai, action := range g.EntityActions {
		if t < len(g.TimedTicks) && g.TimedTicks[t] == ai {
			t++
		}

		et := EntityTick{
			Action: action.Action,
		}

		if et.Action == Update {
			et.Data = g.EntityData[di]
			di++
		}

		ticks[t].Entities[action.EntityID] = et
	}

	return ticks
}
