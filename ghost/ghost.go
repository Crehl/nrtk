package ghost

import (
	"encoding/binary"
	"io"
)

type Action int16

const (
	Create   Action = 0
	Remove   Action = 1
	Update   Action = 2
	BeginFly Action = 3
	EndFly   Action = 4
)

type Ghost struct {
	MagicNumber   int32
	EntityActions []EntityAction
	TimedTicks    []int32
	EntityTypes   []int16
	EntityData    []EntityData
	EntityNames   []string
}

type EntityAction struct {
	Action   Action
	EntityID int16
}

type EntityData struct {
	Rotation Quaternion
	Location Vec3
}

type Quaternion struct {
	W, X, Y, Z float32
}

type Vec3 struct {
	X, Y, Z float32
}

func (g *Ghost) Time() float32 {
	return float32(len(g.TimedTicks)) * 0.06
}

func New(r io.Reader) (g *Ghost, err error) {
	err = binary.Read(r, binary.LittleEndian, &g.MagicNumber)
	if err != nil {
		return
	}

	var numActions int32
	err = binary.Read(r, binary.LittleEndian, &numActions)
	if err != nil {
		return
	}
	g.EntityActions = make([]EntityAction, numActions)
	err = binary.Read(r, binary.LittleEndian, &g.EntityActions)
	if err != nil {
		return
	}

	var numTimedTicks int32
	err = binary.Read(r, binary.LittleEndian, &numTimedTicks)
	if err != nil {
		return
	}
	g.TimedTicks = make([]int32, numTimedTicks)
	err = binary.Read(r, binary.LittleEndian, &g.TimedTicks)
	if err != nil {
		return
	}

	var numEntities int32
	err = binary.Read(r, binary.LittleEndian, &numEntities)
	if err != nil {
		return
	}
	g.EntityTypes = make([]int16, numEntities)
	err = binary.Read(r, binary.LittleEndian, &g.EntityTypes)
	if err != nil {
		return
	}

	var numData int32
	err = binary.Read(r, binary.LittleEndian, &numData)
	if err != nil {
		return
	}
	g.EntityData = make([]EntityData, numData)
	err = binary.Read(r, binary.LittleEndian, &g.EntityData)
	if err != nil {
		return
	}

	var numStrings int32
	err = binary.Read(r, binary.LittleEndian, &numStrings)
	if err != nil {
		return
	}

	g.EntityNames = make([]string, numStrings)
	for i := 0; int32(i) < numStrings; i++ {
		var strLength int32
		err = binary.Read(r, binary.LittleEndian, &strLength)

		buf := make([]byte, strLength)
		_, err = r.Read(buf)
		if err != nil {
			return
		}

		g.EntityNames[i] = string(buf[:strLength-1])
	}

	return g, nil
}
