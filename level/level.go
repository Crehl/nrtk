package level

import (
	"strings"
)

type ScoreStyle int

const (
	None  ScoreStyle = 0
	Time  ScoreStyle = 1
	Score ScoreStyle = 2
	Trick ScoreStyle = 4
)

type LevelType int

const (
	Campaign  LevelType = 0
	StuntMode LevelType = 1
	Tutorial  LevelType = 2
)

func (s ScoreStyle) SqlOrdering() string {
	var orderings []string

	if s&Time > 0 {
		orderings = append(orderings, "time ASC")
	}
	if s&Score > 0 {
		orderings = append(orderings, "score DESC")
	}
	if s&Trick > 0 {
		orderings = append(orderings, "bestTrick DESC")
	}

	order := strings.Join(orderings, ", ")
	if order != "" {
		return "ORDER BY " + order
	}

	return ""
}
