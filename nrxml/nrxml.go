package nrxml

type Node struct {
	Booleans []Boolean `xml:"Boolean"`
	Floats   []Float   `xml:"Float"`
	Integers []Integer `xml:"Integer"`
	Strings  []String  `xml:"String"`
}

type Boolean struct {
	Name  string `xml:"name,attr"`
	Value bool   `xml:"value,attr"`
}

type Float struct {
	Name  string  `xml:"name,attr"`
	Value float32 `xml:"value,attr"`
}

type Integer struct {
	Name  string `xml:"name,attr"`
	Value int    `xml:"value,attr"`
}

type String struct {
	Name  string `xml:"name,attr"`
	Value string `xml:",chardata"`
}

func SetBoolean(booleans []Boolean, name string, out *bool) {
	for _, boolean := range booleans {
		if boolean.Name == name {
			*out = boolean.Value
			return
		}
	}
}

func SetFloat(floats []Float, name string, out *float32) {
	for _, float := range floats {
		if float.Name == name {
			*out = float.Value
			return
		}
	}
}

func SetInteger(integers []Integer, name string, out *int) {
	for _, integer := range integers {
		if integer.Name == name {
			*out = integer.Value
			return
		}
	}
}

func SetString(strings []String, name string, out *string) {
	for _, str := range strings {
		if str.Name == name {
			*out = str.Value
			return
		}
	}
}
